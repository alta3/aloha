from aiohttp import web


def setup(app):
    app.add_routes([web.get('/aloha', alo), web.get('/', hi)])


async def hi(request):
    print("hi")
    resp = web.Response(text="Please try out the /aloha path!")


async def alo(request):
    print("Aloha yourself!")
    resp = web.Response(text='Aloha means "hi"')
    return resp


if __name__ == "__main__":
    print("This Hawaiian Translator App is starting up!")
    app = web.Application()
    setup(app)
    web.run_app(app, port=54321)
