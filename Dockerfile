FROM python:slim

COPY aloha.py /home/ubuntu/aloha.py

RUN pip3 install aiohttp

CMD ["python3", "/home/ubuntu/aloha.py"]
EXPOSE 54321
